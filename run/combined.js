(function () {
function AffectedBalls (movingBall, matchBalls, steadyBalls) {

    function include (x, y) {

        for (var i = 0; i < explodingBalls.length; i++) {
            var explodingBall = explodingBalls[i]
            if (explodingBall.x === x && explodingBall.y === y) return
        }

        var key = x + '-' + y
        if (checked[key]) return

        checked[key] = true

        for (var i = 0; i < steadyBalls.length; i++) {
            var steadyBall = steadyBalls[i]
            if (steadyBall.x === x && steadyBall.y === y) {
                affectedBalls.push(steadyBall)
                explodingBalls.push(steadyBall)
                return
            }
        }

    }

    var explodingBalls = matchBalls.slice(0)
    explodingBalls.push(movingBall)

    var checked = {}

    var affectedBalls = []

    while (explodingBalls.length > 0) {
        var explodingBall = explodingBalls.shift()
        var feature = explodingBall.feature
        if (feature && feature.isExplosive) {

            var x = explodingBall.x,
                y = explodingBall.y

            include(x - 1, y - 1)
            include(x - 1, y)
            include(x - 1, y + 1)
            include(x, y - 1)
            include(x, y)
            include(x, y + 1)
            include(x + 1, y - 1)
            include(x + 1, y)
            include(x + 1, y + 1)

        }
    }

    return affectedBalls

}
;
function AvailableColor () {

    var colors = [{
        name: 'blue',
        value: '#2547f4', // 'hsl(230, 90%, 55%)',
    }, {
        name: 'cyan',
        value: '#17cfcf', // 'hsl(180, 80%, 45%)',
    }, {
        name: 'green',
        value: '#36cf17', // 'hsl(110, 80%, 45%)',
    }, {
        name: 'violet',
        value: '#ab30e8', // 'hsl(280, 80%, 55%)',
    }, {
        name: 'red',
        value: '#e83030', // 'hsl(0, 80%, 55%)',
    }]

    return {
        getByName: function (name) {
            return colors.filter(function (color) {
                return color.name === name
            })[0]
        },
        getRandom: function () {
            return colors[Math.floor(Math.random() * colors.length)]
        },
    }

}
;
function AvailableFeature (boardSize, numberOfBalls) {
    return {
        getByName: function (name) {
            if (name === 'explosive') return Explosive(boardSize, numberOfBalls)
            if (name === 'locked') return Locked(boardSize, numberOfBalls)
            return null
        },
        getRandom: function () {
            var random = Math.random()
            if (random < 0.10) return Explosive(boardSize, numberOfBalls)
            if (random < 0.12) return Locked(boardSize, numberOfBalls)
            return null
        },
        resize: function (_boardSize) {
            boardSize = _boardSize
        },
    }
}
;
function Board (boardSize, numberOfBalls) {

    function addLine (c, i) {

        c.moveTo(-halfBoardSize, -halfBoardSize + cellSize * i)
        c.lineTo(halfBoardSize, -halfBoardSize + cellSize * i)

        c.moveTo(-halfBoardSize + cellSize * i, -halfBoardSize)
        c.lineTo(-halfBoardSize + cellSize * i, halfBoardSize)

    }

    function update () {

        cellSize = boardSize / numberOfBalls
        halfBoardSize = boardSize / 2

        canvasSize = Math.ceil(boardSize * 1.01)
        halfCanvasSize = canvasSize / 2
        canvas.width = canvas.height = canvasSize

        var c = canvas.getContext('2d')
        c.translate(halfCanvasSize, halfCanvasSize)

        c.beginPath()
        for (var i = 1; i < numberOfBalls; i++) addLine(c, i)
        c.lineWidth = boardSize * 0.003
        c.strokeStyle = '#333'
        c.stroke()

        c.beginPath()
        addLine(c, 0)
        addLine(c, numberOfBalls)
        c.lineWidth = boardSize * 0.005
        c.strokeStyle = '#666'
        c.stroke()

    }

    var cellSize, halfBoardSize, canvasSize, halfCanvasSize

    var canvas = document.createElement('canvas')

    update()

    return {
        paint: function (c) {
            c.drawImage(canvas, -halfCanvasSize, -halfCanvasSize)
        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function DrawBall (c, x, y, radius, color, feature) {

    var pi = Math.PI

    c.beginPath()
    c.arc(x, y, radius, 0, pi * 2)
    c.fillStyle = color
    c.fill()

    if (feature !== null) feature.paint(c, x, y, radius)

    var reflectionRadius = radius * 0.64

    c.beginPath()
    c.moveTo(x, y - reflectionRadius)
    c.arc(x, y, reflectionRadius, -pi * 0.5, -pi * 0.25)
    c.strokeStyle = 'white'
    c.lineWidth = radius * 0.2
    c.stroke()

}
;
function EmptySlot (x, y) {
    return {
        x: x,
        y: y,
    }
}
;
function ExplodedBalls (balls, boardSize, numberOfBalls) {

    function update () {
        cellSize = boardSize / numberOfBalls
        radius = (cellSize / 2) * 0.68
        offset = -boardSize / 2 + cellSize / 2
    }

    var offsetTime = Date.now()

    var cellSize, radius, offset

    var particles = []
    for (var i = 0; i < balls.length; i++) {
        var ball = balls[i]
        for (var j = 0; j < 4; j++) {
            particles.push(Particle(ball.x, ball.y, ball.color.value))
        }
    }

    update()

    return {
        paint: function (c) {

            var ratio = (Date.now() - offsetTime) / 500
            if (ratio > 1) return false

            c.save()
            c.translate(offset, offset)

            for (var i = 0; i < particles.length; i++) {
                particles[i].paint(c, ratio, cellSize)
            }

            c.restore()

            return true

        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function ExplodingAllBalls (balls, boardSize, numberOfBalls) {

    function drawBall (c, ball, ballRadius) {
        DrawBall(c, ball.x * cellSize, ball.y * cellSize, ballRadius, ball.color.value, ball.feature)
    }

    function update () {
        cellSize = boardSize / numberOfBalls
        radius = (cellSize / 2) * 0.68
        offset = -boardSize / 2 + cellSize / 2
        wideRadius = radius * 0.3
    }

    var offsetTime = Date.now()

    var cellSize, radius, wideRadius, offset

    update()

    return {
        balls: balls,
        paint: function (c) {

            var ratio = (Date.now() - offsetTime) / 500
            if (ratio > 1) return false

            c.save()
            c.translate(offset, offset)

            var ballRadius = radius + ratio * wideRadius
            for (var i = 0; i < balls.length; i++) {
                drawBall(c, balls[i], ballRadius)
            }

            c.restore()

            return true

        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function ExplodingBalls (movingBall, affectedBalls, balls, boardSize, numberOfBalls) {

    function drawBall (c, ball, ballRadius) {
        DrawBall(c, ball.x * cellSize, ball.y * cellSize, ballRadius, ball.color.value, ball.feature)
    }

    function update () {
        cellSize = boardSize / numberOfBalls
        radius = (cellSize / 2) * 0.68
        offset = -boardSize / 2 + cellSize / 2
        wideRadius = radius * 0.3
    }

    var offsetTime = Date.now()

    var cellSize, radius, wideRadius, offset

    update()

    return {
        affectedBalls: affectedBalls,
        balls: balls,
        movingBall: movingBall,
        paint: function (c) {

            var ratio = (Date.now() - offsetTime) / 500
            if (ratio > 1) return false

            c.save()
            c.translate(offset, offset)

            var ballRadius = radius + ratio * wideRadius
            for (var i = 0; i < balls.length; i++) {
                drawBall(c, balls[i], ballRadius)
            }
            drawBall(c, movingBall, ballRadius)

            c.restore()

            return true

        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function Explosive (boardSize, numberOfBalls) {

    function update () {
        cellSize = boardSize / numberOfBalls
        bigRadius = cellSize * 0.27
        smallRadius = cellSize * 0.12
        centerRadius = cellSize * 0.07
    }

    var cellSize,
        bigRadius,
        smallRadius,
        centerRadius,
        angle = Math.PI / 3

    update()

    return {
        name: 'explosive',
        isExplosive: true,
        paint: function (c, x, y, radius) {

            c.save()
            c.translate(x, y)

            var scale = radius / (cellSize * 0.32)

            c.beginPath()
            c.lineCap = 'butt'
            for (var i = 0; i < 3; i++) {
                c.moveTo(bigRadius, 0)
                c.arc(0, 0, bigRadius * scale, 0, angle)
                c.lineTo(Math.cos(angle) * smallRadius, Math.sin(angle) * smallRadius)
                c.arc(0, 0, smallRadius * scale, angle, 0, true)
                c.closePath()
                c.rotate(angle * 2)
            }
            c.arc(0, 0, centerRadius * scale, 0, Math.PI * 2)
            c.fillStyle = 'rgba(255, 255, 255, 0.5)'
            c.fill()

            c.restore()

        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function GetPath (fromX, fromY, toX, toY, steadyBalls, numberOfBalls) {

    var checked = {}
    var queue = [{
        x: fromX,
        y: fromY,
        fromX: null,
        fromY: null,
    }]

    var path = null
    var pathFound = false

    while (queue.length) {

        var point = queue.shift()

        var x = point.x,
            y = point.y

        if (x < 0 || y < 0 || x > numberOfBalls - 1 || y > numberOfBalls - 1) continue

        var key = x + '-' + y
        if (checked[key]) continue

        checked[key] = {
            x: point.fromX,
            y: point.fromY,
        }

        var found = false
        for (var i = 0; i < steadyBalls.length; i++) {
            var steadyBall = steadyBalls[i]
            if (steadyBall.x === x && steadyBall.y === y) {
                found = true
                break
            }
        }
        if (found) continue

        if (x === toX && y === toY) {
            pathFound = true
            break
        }

        queue.push({
            x: x + 1,
            y: y,
            fromX: x,
            fromY: y,
        })
        queue.push({
            x: x - 1,
            y: y,
            fromX: x,
            fromY: y,
        })
        queue.push({
            x: x,
            y: y + 1,
            fromX: x,
            fromY: y,
        })
        queue.push({
            x: x,
            y: y - 1,
            fromX: x,
            fromY: y,
        })

    }

    if (pathFound) {
        var path = []
        var x = toX, y = toY
        while (true) {

            var point = checked[x + '-' + y]

            if (point.x === null) break

            path.push({ x: x, y: y })

            x = point.x
            y = point.y

        }
        path.push({ x: fromX, y: fromY })
        path = path.reverse()
    }

    return path

}
;
function Locked (boardSize, numberOfBalls) {

    function update () {
        cellSize = boardSize / numberOfBalls
        lockWidth = cellSize * 0.38
        lockHeight = cellSize * 0.24
        lockY = -cellSize * 0.02
        ringRadius = cellSize * 0.12
        ringY = lockY - cellSize * 0.11
        lineWidth = cellSize * 0.05
    }

    var cellSize, lockWidth, lockHeight, lockY, ringRadius, ringY, lineWidth

    update()

    return {
        name: 'locked',
        isLocked: true,
        paint: function (c, x, y, radius) {

            c.save()
            c.translate(x, y)

            var scale = radius / (cellSize * 0.32)

            c.beginPath()
            c.lineCap = 'butt'
            c.moveTo(-ringRadius * scale, lockY * scale)
            c.lineTo(-ringRadius * scale, ringY * scale)
            c.arc(0, ringY * scale, ringRadius * scale, -Math.PI, 0)
            c.lineTo(ringRadius * scale, lockY * scale)
            c.lineWidth = scale * lineWidth
            c.strokeStyle = 'rgba(255, 255, 255, 0.6)'
            c.stroke()

            c.beginPath()
            c.rect(-lockWidth * scale * 0.5, lockY * scale, lockWidth * scale, lockHeight * scale)
            c.fillStyle = 'rgba(255, 255, 255, 0.6)'
            c.fill()

            c.restore()

        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function MainPanel () {

    function addEmptySlot (x, y) {
        emptySlots.push(EmptySlot(x, y))
    }

    function addNextBall (x, y, color, feature) {
        nextBalls.push(NextBall(x, y, color, feature, boardSize, numberOfBalls))
    }

    function addSteadyBall (x, y, color, feature) {
        steadyBalls.push(SteadyBall(x, y, color, feature, boardSize, numberOfBalls))
    }

    function deselect () {
        addSteadyBall(selectedBall.x, selectedBall.y, selectedBall.color, selectedBall.feature)
        selectedBall = null
    }

    function initEmptySlots () {
        for (var y = 0; y < numberOfBalls; y++) {
            for (var x = 0; x < numberOfBalls; x++) {
                addEmptySlot(x, y)
            }
        }
    }

    function nextStep () {

        shuffle(emptySlots)

        var numBalls = 0

        while (nextBalls.length) {
            var nextBall = nextBalls.shift()
            addSteadyBall(nextBall.x, nextBall.y, nextBall.color, nextBall.feature)
            numBalls++
        }

        while (numBalls < 3) {

            var emptySlot = emptySlots.shift()
            if (!emptySlot) break

            addSteadyBall(emptySlot.x, emptySlot.y, availableColor.getRandom(), availableFeature.getRandom())
            numBalls++

        }

        if (!emptySlots.length) {
            explodingAllBalls = ExplodingAllBalls(steadyBalls.slice(0), boardSize, numberOfBalls)
            steadyBalls.splice(0)
            initEmptySlots()
        }

        for (var i = 0; i < 3; i++) {

            var emptySlot = emptySlots.shift()
            if (!emptySlot) break

            addNextBall(emptySlot.x, emptySlot.y, availableColor.getRandom(), availableFeature.getRandom())

        }

        saveState()

    }

    function paint () {
        if (animationFrame !== null) return
        var startTime = Date.now()
        animationFrame = requestAnimationFrame(function () {

            animationFrame = null
            if (innerWidth !== lastInnerWidth || innerHeight !== lastInnerHeight) resize()
            c.fillStyle = 'rgba(0, 0, 0, 0.8)'
            c.fillRect(0, 0, canvasWidth, canvasHeight)
            c.save()
            c.translate(halfCanvasWidth, halfCanvasHeight)
            paintBoard()
            c.restore()
            score.paint(c)
            pauseButton.paint(c)

            var elapsed = Date.now() - startTime
            if (elapsed > 20) paint()
            else setTimeout(paint, 20 - elapsed)

        })
    }

    function paintBoard () {

        c.lineCap = 'round'

        board.paint(c)

        for (var i = 0; i < steadyBalls.length; i++) {
            steadyBalls[i].paint(c)
        }

        for (var i = 0; i < nextBalls.length; i++) {
            nextBalls[i].paint(c)
        }

        if (selectedBall !== null) selectedBall.paint(c)
        if (movingBall !== null) {
            if (!movingBall.paint(c)) {

                var matchBalls = MatchBalls(movingBall, steadyBalls, numberOfBalls)
                if (matchBalls === null) {

                    for (var i = 0; i < nextBalls.length; i++) {
                        var nextBall = nextBalls[i]
                        if (nextBall.x === movingBall.x && nextBall.y === movingBall.y) {
                            nextBalls.splice(i, 1)
                            break
                        }
                    }

                    for (var i = 0; i < emptySlots.length; i++) {
                        var emptySlot = emptySlots[i]
                        if (emptySlot.x === movingBall.x && emptySlot.y === movingBall.y) {
                            emptySlots.splice(i, 1)
                            break
                        }
                    }

                    addSteadyBall(movingBall.x, movingBall.y, movingBall.color, movingBall.feature)

                    score.add(1)
                    nextStep()

                } else {

                    for (var i = 0; i < matchBalls.length; i++) {
                        var matchBall = matchBalls[i]
                        steadyBalls.splice(steadyBalls.indexOf(matchBall), 1)
                        addEmptySlot(matchBall.x, matchBall.y)
                    }

                    var affectedBalls = AffectedBalls(movingBall, matchBalls, steadyBalls)
                    explodingBalls = ExplodingBalls(movingBall, affectedBalls, matchBalls, boardSize, numberOfBalls)


                }

                movingBall = null

            }
        }
        if (explodingBalls !== null) {
            if (!explodingBalls.paint(c)) {

                var balls = explodingBalls.balls
                balls.push(explodingBalls.movingBall)

                var affectedBalls = explodingBalls.affectedBalls
                for (var i = 0; i < affectedBalls.length; i++) {
                    var affectedBall = affectedBalls[i]
                    steadyBalls.splice(steadyBalls.indexOf(affectedBall), 1)
                    addEmptySlot(affectedBall.x, affectedBall.y)
                    balls.push(affectedBall)
                }

                score.add(balls.length)
                explodedBalls = ExplodedBalls(balls, boardSize, numberOfBalls)
                explodingBalls = null
                saveState()

            }
        }
        if (markedSlots !== null) markedSlots.paint(c)

        if (explodedBalls !== null) {
            if (!explodedBalls.paint(c)) explodedBalls = null
        }

        if (explodingAllBalls !== null) {
            if (!explodingAllBalls.paint(c)) {
                explodedBalls = ExplodedBalls(explodingAllBalls.balls, boardSize, numberOfBalls)
                nextStep()
                explodingAllBalls = null
                score.reset()
                saveState()
            }
        }

    }

    function resize () {

        canvasWidth = innerWidth * devicePixelRatio
        canvasHeight = innerHeight * devicePixelRatio

        halfCanvasWidth = canvasWidth / 2
        halfCanvasHeight = canvasHeight / 2

        canvas.width = Math.floor(canvasWidth)
        canvas.height = Math.floor(canvasHeight)

        boardSize = Math.min(canvasWidth, canvasHeight) * 0.95

        var ratio = canvasWidth / canvasHeight
        if (ratio < 1) ratio = 1 / ratio
        if (ratio < 1.07) boardSize *= 0.95
        if (ratio < 1.12) boardSize *= 0.95
        if (ratio < 1.17) boardSize *= 0.95
        if (ratio < 1.22) boardSize *= 0.95

        halfBoardSize = boardSize / 2

        cellSize = boardSize / numberOfBalls

        score.resize(canvasWidth, canvasHeight, cellSize)
        board.resize(boardSize)
        pauseButton.resize(canvasWidth, boardSize)
        availableFeature.resize(boardSize)

        for (var i = 0; i < steadyBalls.length; i++) {
            steadyBalls[i].resize(boardSize)
        }

        for (var i = 0; i < nextBalls.length; i++) {
            nextBalls[i].resize(boardSize)
        }

        if (selectedBall !== null) selectedBall.resize(boardSize)
        if (movingBall !== null) movingBall.resize(boardSize)
        if (explodingAllBalls !== null) explodingAllBalls.resize(boardSize)
        if (explodingBalls !== null) explodingBalls.resize(boardSize)
        if (explodedBalls !== null) explodedBalls.resize(boardSize)
        if (markedSlots !== null) markedSlots.resize(boardSize)

        canvas.style.top = -halfCanvasHeight + 'px'
        canvas.style.left = -halfCanvasWidth + 'px'
        canvas.style.transform = 'scale(' + (1 / devicePixelRatio) + ')'

        lastInnerWidth = innerWidth
        lastInnerHeight = innerHeight

    }

    function saveState () {
        localStorage.state = JSON.stringify({
            score: score.get(),
            steadyBalls: steadyBalls.map(toStorageBall),
            nextBalls: nextBalls.map(toStorageBall),
        })
    }

    function selectBall (x, y, color, feature) {
        selectedBall = SelectedBall(x, y, color, feature, boardSize, numberOfBalls)
        markedSlots = MarkedSlots(selectedBall, steadyBalls, boardSize, numberOfBalls)
    }

    function shuffle (array) {
        for (var i = 0; i < array.length; i++) {
            var randomIndex = i + Math.floor(Math.random() * (array.length - i))
            var value = array[i]
            array[i] = array[randomIndex]
            array[randomIndex] = value
        }
    }

    function tap (e) {

        if (movingBall !== null || explodingAllBalls !== null || explodingBalls !== null) return

        if (e.clientX * devicePixelRatio > canvasWidth - cellSize &&
            e.clientY * devicePixelRatio < cellSize) {

            if (selectedBall !== null) {
                deselect()
                markedSlots = null
            }

            explodingAllBalls = ExplodingAllBalls(steadyBalls.slice(0), boardSize, numberOfBalls)
            while (steadyBalls.length) {
                var ball = steadyBalls.pop()
                addEmptySlot(ball.x, ball.y)
            }
            return

        }

        var x = Math.floor((e.clientX * devicePixelRatio - halfCanvasWidth + halfBoardSize) / cellSize),
            y = Math.floor((e.clientY * devicePixelRatio - halfCanvasHeight + halfBoardSize) / cellSize)

        if (x < 0 || y < 0 || x > numberOfBalls - 1 || y > numberOfBalls - 1) return

        if (selectedBall === null) {
            for (var i = 0; i < steadyBalls.length; i++) {
                var steadyBall = steadyBalls[i]
                if (steadyBall.x === x && steadyBall.y === y) {

                    var feature = steadyBall.feature
                    if (feature && feature.isLocked) return

                    steadyBalls.splice(i, 1)
                    selectBall(x, y, steadyBall.color, steadyBall.feature)
                    return

                }
            }
        } else {

            if (x === selectedBall.x && y === selectedBall.y) {
                deselect()
                markedSlots = null
                return
            }

            for (var i = 0; i < steadyBalls.length; i++) {
                var steadyBall = steadyBalls[i]
                if (steadyBall.x === x && steadyBall.y === y) {

                    var feature = steadyBall.feature
                    if (feature && feature.isLocked) return

                    deselect()
                    steadyBalls.splice(i, 1)
                    selectBall(x, y, steadyBall.color, steadyBall.feature)
                    return

                }
            }

            var path = GetPath(selectedBall.x, selectedBall.y, x, y, steadyBalls, numberOfBalls)
            if (path) {
                addEmptySlot(selectedBall.x, selectedBall.y)
                movingBall = MovingBall(selectedBall.x, selectedBall.y, x, y, selectedBall.color, selectedBall.feature, path, boardSize, numberOfBalls)
                selectedBall = null
                markedSlots = null
            }

        }

    }

    function toStorageBall (ball) {
        var storageBall = {
            x: ball.x,
            y: ball.y,
            color: ball.color.name,
        }
        if (ball.feature) storageBall.feature = ball.feature.name
        return storageBall
    }

    var score = Score()

    var numberOfBalls = 8

    var lastInnerWidth = null,
        lastInnerHeight = null

    var canvasWidth,
        canvasHeight

    var halfCanvasWidth,
        halfCanvasHeight

    var pauseButton = PauseButton(canvasWidth, boardSize, numberOfBalls)

    var board = Board(boardSize, numberOfBalls)

    var requestAnimationFrame = window.requestAnimationFrame
    if (!requestAnimationFrame) {
        requestAnimationFrame = window.mozRequestAnimationFrame
    }
    if (!requestAnimationFrame) {
        requestAnimationFrame = function (callback) {
            return setTimeout(callback, 0)
        }
    }

    var availableColor = AvailableColor()
    var availableFeature = AvailableFeature(boardSize, numberOfBalls)

    var animationFrame = null

    var emptySlots = []

    var steadyBalls = []
    var nextBalls = []
    var selectedBall = null
    var movingBall = null
    var explodingAllBalls = null
    var explodingBalls = null
    var explodedBalls = null
    var markedSlots = null

    window.steadyBalls = steadyBalls
    window.nextBalls = nextBalls
    window.emptySlots = emptySlots

    var classPrefix = 'MainPanel'

    var touched = false

    var canvas = document.createElement('canvas')
    canvas.className = classPrefix + '-canvas'
    canvas.addEventListener('mousedown', function (e) {

        if (e.button !== 0) return

        if (touched) {
            touched = false
            return
        }

        tap(e)

    })
    canvas.addEventListener('touchstart', function (e) {
        touched = true
        tap(e.changedTouches[0])
    })

    var c = canvas.getContext('2d')

    var centerElement = document.createElement('div')
    centerElement.className = classPrefix + '-center'
    centerElement.appendChild(canvas)

    var element = document.createElement('div')
    element.className = classPrefix
    element.appendChild(centerElement)

    var boardSize,
        halfBoardSize

    var cellSize

    return {
        element: element,
        init: function () {
            var state = localStorage.state
            if (state) {

                var data = JSON.parse(state)
                score.set(data.score)

                data.steadyBalls.forEach(function (ball) {
                    var feature = availableFeature.getByName(ball.feature)
                    var color = availableColor.getByName(ball.color)
                    addSteadyBall(ball.x, ball.y, color, feature)
                })

                data.nextBalls.forEach(function (ball) {
                    var feature = availableFeature.getByName(ball.feature)
                    var color = availableColor.getByName(ball.color)
                    addNextBall(ball.x, ball.y, color, feature)
                })

                for (var y = 0; y < numberOfBalls; y++) {
                    for (var x = 0; x < numberOfBalls; x++) {

                        var empty = true

                        for (var i = 0; i < steadyBalls.length; i++) {
                            var steadyBall = steadyBalls[i]
                            if (steadyBall.x === x && steadyBall.y === y) {
                                empty = false
                                break
                            }
                        }

                        if (!empty) continue

                        for (var i = 0; i < nextBalls.length; i++) {
                            var nextBall = nextBalls[i]
                            if (nextBall.x === x && nextBall.y === y) {
                                empty = false
                                break
                            }
                        }

                        if (empty) addEmptySlot(x, y)
                    }
                }

            } else {
                initEmptySlots()
                nextStep()
            }
            paint()
        },
    }

}
;
function MarkedEdge (fromX, fromY, toX, toY) {
    return {
        fromX: fromX,
        fromY: fromY,
        toX: toX,
        toY: toY,
    }
}
;
function MarkedEdges (selectedBall, slots, boardSize, numberOfBalls) {

    function addEdge (fromX, fromY, toX, toY) {
        edges.push(MarkedEdge(fromX, fromY, toX, toY))
    }

    function isMarked (x, y) {
        for (var i = 0; i < slots.length; i++) {
            var slot = slots[i]
            if (slot.x === x && slot.y === y) return true
        }
        return false
    }

    function update () {
        offset = -boardSize / 2
        cellSize = boardSize / numberOfBalls
    }

    var edges = []

    for (var i = 0; i < slots.length; i++) {

        var slot = slots[i]

        var x = slot.x,
            y = slot.y

        if (!isMarked(x, y - 1)) addEdge(x, y, x + 1, y)
        if (!isMarked(x, y + 1)) addEdge(x, y + 1, x + 1, y + 1)
        if (!isMarked(x - 1, y)) addEdge(x, y, x, y + 1)
        if (!isMarked(x + 1, y)) addEdge(x + 1, y, x + 1, y + 1)

    }

    var offset, cellSize

    update()

    return {
        paint: function (c) {
            c.save()
            c.translate(offset, offset)
            c.beginPath()
            for (var i = 0; i < edges.length; i++) {
                var edge = edges[i]
                c.moveTo(edge.fromX * cellSize, edge.fromY * cellSize)
                c.lineTo(edge.toX * cellSize, edge.toY * cellSize)
            }
            c.lineWidth = boardSize * 0.005
            c.lineCap = 'round'
            c.strokeStyle = selectedBall.color.value
            c.stroke()
            c.restore()
        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function MarkedSlot (x, y) {
    return {
        x: x,
        y: y,
    }
}
;
function MarkedSlots (selectedBall, steadyBalls, boardSize, numberOfBalls) {

    function update () {

        offset = -boardSize / 2
        cellSize = boardSize / numberOfBalls

        canvasSize = Math.ceil(boardSize * 1.01)
        halfCanvasSize = canvasSize / 2
        canvas.width = canvas.height = canvasSize

        var c = canvas.getContext('2d')
        c.translate(halfCanvasSize, halfCanvasSize)

        c.save()
        c.translate(offset, offset)
        c.globalAlpha = 0.2
        c.beginPath()
        for (var i = 0; i < slots.length; i++) {
            var slot = slots[i]
            c.rect(slot.x * cellSize, slot.y * cellSize, cellSize, cellSize)
        }
        c.fillStyle = selectedBall.color.value
        c.fill()
        c.restore()

        edges.paint(c)

    }

    var canvas = document.createElement('canvas')

    var checked = {}
    var queue = [{
        x: selectedBall.x,
        y: selectedBall.y,
    }]

    var slots = []

    while (queue.length) {

        var point = queue.shift()

        var x = point.x,
            y = point.y

        if (x < 0 || y < 0 || x > numberOfBalls - 1 || y > numberOfBalls - 1) continue

        var key = x + '-' + y
        if (checked[key]) continue

        checked[key] = true

        var found = false
        for (var i = 0; i < steadyBalls.length; i++) {
            var steadyBall = steadyBalls[i]
            if (steadyBall.x === x && steadyBall.y === y) {
                found = true
                break
            }
        }
        if (found) continue

        slots.push(MarkedSlot(x, y))

        queue.push({
            x: x + 1,
            y: y,
        })
        queue.push({
            x: x - 1,
            y: y,
        })
        queue.push({
            x: x,
            y: y + 1,
        })
        queue.push({
            x: x,
            y: y - 1,
        })

    }

    var offset, cellSize, canvasSize, halfCanvasSize

    var edges = MarkedEdges(selectedBall, slots, boardSize, numberOfBalls)

    update()

    return {
        paint: function (c) {
            c.drawImage(canvas, -halfCanvasSize, -halfCanvasSize)
        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            edges.resize(boardSize)
            update()
        },
    }

}
;
function MatchBalls (movingBall, steadyBalls, numberOfBalls) {

    function match (dx1, dy1, dx2, dy2) {

        function include (dx, dy) {

            var x = movingBall.x,
                y = movingBall.y

            while (true) {

                x += dx
                y += dy

                if (x < 0 || y < 0 || x > numberOfBalls - 1 || y > numberOfBalls - 1) break

                var found = false
                for (var i = 0; i < steadyBalls.length; i++) {
                    var steadyBall = steadyBalls[i]
                    if (steadyBall.x === x && steadyBall.y === y) {
                        if (steadyBall.color === movingBall.color) {
                            matched.push(steadyBall)
                            found = true
                        }
                        break
                    }
                }
                if (!found) break

            }

        }

        var matched = []
        include(dx1, dy1)
        include(dx2, dy2)
        if (matched.length > 3) {
            matchBalls.push.apply(matchBalls, matched)
        }

    }

    var matchBalls = []

    match(-1, 0, 1, 0)
    match(0, -1, 0, 1)
    match(-1, -1, 1, 1)
    match(-1, 1, 1, -1)

    if (matchBalls.length) return matchBalls
    return null

}
;
function MovingBall (x, y, toX, toY, color, feature, path, boardSize, numberOfBalls) {

    function update () {
        cellSize = boardSize / numberOfBalls
        offset = -boardSize / 2 + boardSize / (2 * numberOfBalls)
        radius = cellSize * 0.14
    }

    var point = path.shift()

    var index = 0

    var offset,
        cellSize,
        radius

    update()

    return {
        color: color,
        feature: feature,
        x: toX,
        y: toY,
        paint: function (c) {

            index++
            if (index === 2) {
                index = 0
                point = path.shift()
                if (!point) return false
            }

            x = point.x
            y = point.y

            visualX = offset + x * cellSize
            visualY = offset + y * cellSize

            DrawBall(c, visualX, visualY, radius, color.value, null)

            return true

        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function NextBall (x, y, color, feature, boardSize, numberOfBalls) {

    function update () {

        var cellSize = boardSize / numberOfBalls
        radius = cellSize * 0.14

        var offset = -boardSize / 2 + boardSize / (2 * numberOfBalls)
        visualX = offset + x * cellSize
        visualY = offset + y * cellSize

    }

    var radius
    var visualX, visualY

    update()

    return {
        color: color,
        feature: feature,
        x: x,
        y: y,
        paint: function (c) {
            DrawBall(c, visualX, visualY, radius, color.value, null)
        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function Particle (x, y, color) {

    var pi = Math.PI

    var multiplier = 0.008
    var randomAngle = Math.random() * pi * 2
    x += Math.cos(randomAngle) * multiplier
    y += Math.sin(randomAngle) * multiplier

    var multiplier = 0.5
    var randomAngle = Math.random() * pi * 2
    var distanceX = Math.cos(randomAngle) * multiplier,
        distanceY = Math.sin(randomAngle) * multiplier

    var speedY = -Math.random() * 0.04

    return {
        paint: function (c, ratio, cellSize) {
            c.beginPath()
            c.arc(x * cellSize + cellSize * distanceX * ratio, y * cellSize + cellSize * distanceY * ratio, cellSize * 0.2 * (1 - ratio), 0, pi * 2)
            c.fillStyle = color
            c.fill()
            y += speedY
        },
    }

}
;
function PauseButton (canvasWidth, boardSize, numberOfBalls) {

    function update () {
        cellSize = boardSize / numberOfBalls
        width = cellSize * 0.12
        height = cellSize * 0.4
        visualX = canvasWidth - width * 3.2 - cellSize * 0.25
        visualY = cellSize * 0.25
    }

    var cellSize, width, height, visualX, visualY

    update()

    return {
        paint: function (c) {
            c.save()
            c.strokeStyle = '#808080'
            c.beginPath()
            c.translate(canvasWidth - cellSize * 0.5, cellSize * 0.5)
            c.arc(0, 0, cellSize * 0.2, -Math.PI, Math.PI * 0.25)
            c.moveTo(-cellSize * 0.26, -cellSize * 0.2)
            c.lineTo(-cellSize * 0.26, 0)
            c.lineTo(-cellSize * 0.06, 0)
            c.lineWidth = cellSize * 0.08
            c.stroke()
            c.restore()
        },
        resize: function (_canvasWidth, _boardSize) {
            canvasWidth = _canvasWidth
            boardSize = _boardSize
            update()
        },
    }

}
;
function Score () {

    function highlightHighest (c) {
        if (highestHighlight > 0) highestHighlight--
        if (highestHighlight > 0) c.fillStyle = highlightFillStyle
    }

    function highlightValue (c) {
        if (valueHighlight > 0) valueHighlight--
        if (valueHighlight > 0) c.fillStyle = highlightFillStyle
    }

    var value = 0

    var valueHighlight = 0
    var highestHighlight = 0
    var maxHighlight = 50
    var highlightFillStyle = '#f7f76e'

    var fontPadding

    var highest = parseInt(localStorage.highScore, 10)
    if (!isFinite(highest)) highest = 0

    var canvasWidth, canvasHeight, cellSize

    return {
        add: function (n) {
            value += n
            valueHighlight = maxHighlight
            if (value > highest) {
                highest = value
                highestHighlight = maxHighlight
                localStorage.highScore = highest
            }
        },
        get: function () {
            return value
        },
        paint: function (c) {

            c.save()

            var smallFontSize = cellSize * 0.26,
                largeFontSize = cellSize * 0.5,
                spacing = cellSize * 0.1

            var smallFont = 'normal ' + smallFontSize + 'px sans-serif',
                largeFont = 'normal ' + cellSize * 0.5 + 'px sans-serif'

            c.fillStyle = '#808080'
            c.textBaseline = 'top'
            if (canvasWidth > canvasHeight) {

                c.save()
                highlightValue(c)
                c.translate(fontPadding, fontPadding)
                c.textAlign = 'right'
                c.rotate(-Math.PI * 0.5)
                c.font = smallFont
                c.fillText('YOUR SCORE', 0, 0)
                c.font = largeFont
                c.fillText(value, 0, smallFontSize + spacing)
                c.restore()

                c.save()
                c.translate(fontPadding, canvasHeight - fontPadding)
                c.textAlign = 'left'
                c.rotate(-Math.PI * 0.5)
                c.font = smallFont
                highlightHighest(c)
                c.fillText('HIGH SCORE', 0, 0)
                c.font = largeFont
                c.fillText(highest, 0, smallFontSize + spacing)
                c.restore()

            } else {

                c.save()
                highlightValue(c)
                c.font = smallFont
                c.fillText('YOUR SCORE', fontPadding, fontPadding)
                c.font = largeFont
                c.fillText(value, fontPadding, fontPadding + smallFontSize + spacing)
                c.restore()

                c.save()
                highlightHighest(c)
                c.font = smallFont
                c.fillText('HIGH SCORE', fontPadding, canvasHeight - fontPadding - largeFontSize - spacing - smallFontSize)
                c.font = largeFont
                c.fillText(highest, fontPadding, canvasHeight - fontPadding - largeFontSize)
                c.restore()

            }

            c.restore()

        },
        reset: function () {
            value = 0
        },
        resize: function (_canvasWidth, _canvasHeight, _cellSize) {
            canvasWidth = _canvasWidth
            canvasHeight = _canvasHeight
            cellSize = _cellSize
            fontPadding = cellSize * 0.25
        },
        set: function (_value) {
            value = _value
        },
    }

}
;
function SelectedBall (x, y, color, feature, boardSize, numberOfBalls) {

    function update () {

        var cellSize = boardSize / numberOfBalls
        radius = cellSize * 0.34

        var offset = -boardSize / 2 + boardSize / (2 * numberOfBalls)
        visualX = offset + x * cellSize
        visualY = offset + y * cellSize

    }

    var offsetTime = Date.now()

    var radius
    var visualX, visualY

    update()

    return {
        color: color,
        feature: feature,
        x: x,
        y: y,
        paint: function (c) {

            var sin = Math.sin((Date.now() - offsetTime) * 0.008)
            var jumpY = sin
            if (sin < 0) jumpY *= 0.3

            c.save()
            c.translate(visualX, visualY - jumpY * radius * 0.4)
            if (sin < 0) {
                var scale = sin * 0.15
                c.scale(1 - scale, 1 + scale)
            }
            DrawBall(c, 0, 0, radius, color.value, feature)
            c.restore()

        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
        },
    }

}
;
function SteadyBall (x, y, color, feature, boardSize, numberOfBalls) {

    function update () {

        var cellSize = boardSize / numberOfBalls
        radius = cellSize * 0.34

        var offset = -boardSize / 2 + boardSize / (2 * numberOfBalls)
        visualX = offset + x * cellSize
        visualY = offset + y * cellSize

    }

    var radius
    var visualX, visualY

    update()

    return {
        color: color,
        feature: feature,
        x: x,
        y: y,
        paint: function (c) {
            DrawBall(c, visualX, visualY, radius, color.value, feature)
        },
        resize: function (_boardSize) {
            boardSize = _boardSize
            update()
            if (feature !== null) feature.resize(boardSize)
        },
    }

}
;
(function () {

    var mainPanel = MainPanel()

    document.body.appendChild(mainPanel.element)
    mainPanel.init()

})()
;

})()