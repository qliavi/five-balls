<?php

function get_run_revisions () {
    return [
        'compressed.css' => 1,
        'compressed.js' => 10,
    ];
}
