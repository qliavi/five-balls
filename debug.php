<?php

include_once 'fns/echo_html.php';
echo_html(
    '<html>',
    '<link rel="stylesheet" type="text/css" href="css/Main.css" />'
    .'<link rel="stylesheet" type="text/css" href="css/MainPanel.css" />',
    '<script type="text/javascript" src="js/AffectedBalls.js"></script>'
    .'<script type="text/javascript" src="js/AvailableColor.js"></script>'
    .'<script type="text/javascript" src="js/AvailableFeature.js"></script>'
    .'<script type="text/javascript" src="js/Board.js"></script>'
    .'<script type="text/javascript" src="js/DrawBall.js"></script>'
    .'<script type="text/javascript" src="js/EmptySlot.js"></script>'
    .'<script type="text/javascript" src="js/ExplodedBalls.js"></script>'
    .'<script type="text/javascript" src="js/ExplodingAllBalls.js"></script>'
    .'<script type="text/javascript" src="js/ExplodingBalls.js"></script>'
    .'<script type="text/javascript" src="js/Explosive.js"></script>'
    .'<script type="text/javascript" src="js/GetPath.js"></script>'
    .'<script type="text/javascript" src="js/Locked.js"></script>'
    .'<script type="text/javascript" src="js/MainPanel.js"></script>'
    .'<script type="text/javascript" src="js/MarkedEdge.js"></script>'
    .'<script type="text/javascript" src="js/MarkedEdges.js"></script>'
    .'<script type="text/javascript" src="js/MarkedSlot.js"></script>'
    .'<script type="text/javascript" src="js/MarkedSlots.js"></script>'
    .'<script type="text/javascript" src="js/MatchBalls.js"></script>'
    .'<script type="text/javascript" src="js/MovingBall.js"></script>'
    .'<script type="text/javascript" src="js/NextBall.js"></script>'
    .'<script type="text/javascript" src="js/Particle.js"></script>'
    .'<script type="text/javascript" src="js/PauseButton.js"></script>'
    .'<script type="text/javascript" src="js/Score.js"></script>'
    .'<script type="text/javascript" src="js/SelectedBall.js"></script>'
    .'<script type="text/javascript" src="js/SteadyBall.js"></script>'
    .'<script type="text/javascript" src="js/Main.js"></script>'
);
