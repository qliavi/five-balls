Five Balls
==========

A game where you take a ball and put it in another place.
When five or more balls align in horizontal,
in vertical or in diagonal they break.

The app is available at http://five-balls.qliavi.com/.

For technical support create an issue or contact us at info@qliavi.com.
